
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--Bootstrap CSS-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<!--Bootstrap JS-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
h1, h2, h3, h4, h5, h6 {
	color: #25292a;
	margin: 0px 0px 10px 0px;
	font-family: 'Overpass', sans-serif;
	font-weight: 700;
	letter-spacing: -1px;
	line-height: 1;
}

p {
	margin: 0 0 20px;
	line-height: 1.7;
}

.page-header {
	background:
		url(https://images7.alphacoders.com/106/thumb-1920-1062176.jpg)
		no-repeat;
	position: relative;
	background-size: cover;
}

.page-caption {
	padding-top: 60%;
}

.page-title {
	font-size: 30px;
	line-height: 1;
	color: #000;
	font-weight: 0;
}

.card-section {
	position: relative;
	bottom: 60px;
}

.card-block {
	padding: 80px;
}

.section-title {
	margin-bottom: 60px;
}

h1, h2, h3, h4, h5 {
	color: black
}

tr {
	color: black
}

td {
	color: black
}

ul {
	color: black
}

li {
	color: black
}

label {
	color: black
}

p {
	color: black
}

a {
	color: black
}

nav {
	width: %100;
}

.topnav a:hover {
	border-bottom: 3px solid red;
}

#sailorTableArea {
	max-height: 700px;
	overflow-x: auto;
	overflow-y: auto;
}

#sailorTableArea {
	max-height: 700px;
	overflow-x: auto;
	overflow-y: auto;
}

#sailorTable {
	white-space: normal;
}

tbody {
	display: block;
	height: 700px;
	overflow: auto;
}

thead, tbody tr {
	display: table;
	width: 100%;
	table-layout: fixed;
}

.full-height {
  height: 100%;
}

.modal { 
    width: 370px;
    margin: auto;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: grey;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}

</style>