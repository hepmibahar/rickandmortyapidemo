<jsp:include page="HeaderMenu.jsp" />
<!-- page-header -->
<div class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-caption">
				
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.page-header-->
<div class="card-section">
	<div class="container">
		<div class="jumbotron">
			<h5 class="display-5">EGEMSOFT DEMO PROJECT</h5>
			<p class="lead">This project has been developed using Spring Boot
				technology. While developing the project, Rick and Morty API was
				used in the project.</p>
			<hr class="my-4">
			<p>Rick and Morty API is a RESTful API based on the television
				show Rick and Morty. Hundreds of characters, images, locations and
				sections are accessible.</p>
			<p class="lead"></p>
		</div>

	</div>
</div>

<jsp:include page="Footer.jsp" />
