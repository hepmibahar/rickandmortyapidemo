<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="HeaderMenu.jsp" />
<jsp:include page="PageInfo.jsp" />
<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr class="thead-dark">
					<th><button class="btn-success">
							<i class="fa fa-bars"></i>
						</button></th>
					<th>Name
						<a href="/locationSorting/"><button class="btn btn-info">
							<i class="fa fa-sort-alpha-asc" aria-hidden="true"></i>
						</button></a>
					</th>
					<th>Type</th>
					<th>Dimension</th>
					<th>Created</th>
					<th>Detail</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${LocationList}" var="i">
					<tr>
						<td>${i.id}</td>
						<td>${i.name}</td>
						<td>${i.type}</td>
						<td>${i.dimension}</td>
						<td>${i.created}</td>
						<td><a href="/locations/${i.id}"><button
									class="btn btn-warning">
									<i class="fa fa-folder"></i>
								</button></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>


	</div>
	<div class="container" style="heigh: 100px">
		<div>
			<div style="float: left">
				<a href="/locationsPrev/"><button class="btn btn-warning">
						<i class="fa fa-arrow-circle-left">Prev Page</i>
					</button></a>
			</div>
			<div style="float: right">
				<a href="/locationsNext/"><button class="btn btn-warning">
						<i class="fa fa-arrow-circle-right">Next Page</i>
					</button></a>

			</div>
		</div>
	</div>
</div>

<div class="container" style="heigh: 100px">
	<h1></h1>
	<br />
</div>

<jsp:include page="Footer.jsp" />
