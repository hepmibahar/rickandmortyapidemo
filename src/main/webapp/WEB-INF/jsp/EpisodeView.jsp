<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="HeaderMenu.jsp" />
<jsp:include page="PageInfo.jsp" />
<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr class="thead-dark">
					<th><button class="btn-success">
							<i class="fa fa-bars"></i>
						</button></th>
					<th>Name
						<a href="/episodeSorting/"><button class="btn btn-info">
							<i class="fa fa-sort-alpha-asc" aria-hidden="true"></i>
						</button></a>
					</th>
					<th>Air Date</th>
					<th>Episode</th>
					<th>Created</th>					
					<th>Detail</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${EpisodeList}" var="i">
					<tr>
						<td>${i.id}<br></td>
						<td>${i.name}<br></td>
						<td>${i.air_date}<br></td>
						<td>${i.episode}<br></td>
						<td>${i.created}<br></td>						
						<td><a href="/episodes/${i.id}"><button class="btn btn-warning">
								<i class="fa fa-folder"></i>
							</button></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>


	</div>
	<div class="container" style="heigh: 100px">
		<div>
			<div style="float: left">
				<a href="/episodesPrev/"><button class="btn btn-warning">
						<i class="fa fa-arrow-circle-left">Prev Page</i>						
					</button></a>					
			</div>
			<div style="float: right">
				<a href="/episodesNext/"><button class="btn btn-warning">
						<i class="fa fa-arrow-circle-right">Next Page</i>
					</button></a>
					
			</div>
		</div>
	</div>
</div>

<div class="container" style="heigh: 100px">
	<h1></h1>
	<br />
</div>

<jsp:include page="Footer.jsp" />
