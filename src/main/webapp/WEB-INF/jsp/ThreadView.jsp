<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="HeaderMenu.jsp" />
<th/>
<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr class="thead-dark">
					<th>Character</th>
					<th>Count</th>
				</tr>
			</thead>
			<tr>
					<td>Total Character Count</td>
					<td>${totalCount}</td>
				</tr>
			<c:forEach var="entry" items="${AlphCharacterList}">				
				<tr>
					<td><c:out value="${entry.key}" /></td>
					<td><c:out value="${entry.value}" /></td>
				</tr>
			</c:forEach>
		</table>
	</div>


</div>

<div class="container" style="heigh: 100px">
	<h1></h1>
	<br />
</div>

<jsp:include page="Footer.jsp" />
