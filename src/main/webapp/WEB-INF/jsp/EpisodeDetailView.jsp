<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="HeaderMenu.jsp" />
<jsp:include page="PageDetailInfo.jsp" />
<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr class="thead-dark">
					<th><button class="btn btn-primay">
							<i class="fa fa-bars"></i>
						</button></th>
						<th>Id</th>
					<th>Name</th>
					<th>Air Date</th>
					<th>Episode</th>
					<th>Created</th>
					
				</tr>
			</thead>
			<tbody style="height: 100px;">
				<tr>
				<td></td>
					<td>${Episode.id}</td>
					<td>${Episode.name}</td>
					<td>${Episode.air_date}</td>
					<td>${Episode.episode}</td>
					<td>${Episode.created}</td>					
				</tr>
			</tbody>			
			<thead>
				<tr class="thead-primary">
					
					<th><button class="btn ">
							<i class="fa fa-slideshare" aria-hidden="true"></i>
						</button> Characters</th>					
					<th>Detail</th>

				</tr>
			</thead>
			<tbody style="height: 350px;">
				<c:forEach items="${Characters}" var="listItem">
					<tr>
					<td><i class="fa fa-caret-right fa-lg" aria-hidden="true"></i>
						${listItem}</td>
						<td><button class="btn btn-warning">
								<i class="fa fa-folder"></i>
							</button> <br></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="container" style="heigh: 100px">
	<h1></h1>
	<br />
</div>

<jsp:include page="Footer.jsp" />
