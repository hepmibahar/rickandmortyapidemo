<nav class="navbar navbar-expand-lg navbar-light bg-light">

	<a class="navbar-brand" href="${contextPath}/">Rick And Morty API</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarColor03" aria-controls="navbarColor03"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarColor03">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link"
				href="${contextPath}/">
					<button type="button" class="btn btn-dark ">
						<i class="fa fa-home" aria-hidden="true"></i> Main Page <span
							class="sr-only">(current)</span>
					</button>
			</a></li>
			<li class="nav-item active"><a class="nav-link"
				href="${contextPath}/characters">
					<button type="button" class="btn btn-default">
						<i class="fa fa-slideshare" aria-hidden="true"></i> Character <span
							class="sr-only">(current)</span>
					</button>
			</a></li>
			<li class="nav-item active"><a class="nav-link"
				href="${contextPath}/episodes">
					<button type="button" class="btn btn-default ">
						<i class="fa fa-caret-right fa-lg" aria-hidden="true"></i> Episode
						<span class="sr-only">(current)</span>
					</button>
			</a></li>
			<li class="nav-item active"><a class="nav-link"
				href="${contextPath}/locations">
					<button type="button" class="btn btn-default ">
						<i class="fa fa-map-pin" aria-hidden="true"></i> Location <span
							class="sr-only">(current)</span>
					</button>
			</a></li>
			<li class="nav-item active"><a class="nav-link"
				href="${contextPath}/thread-demo">
					<button type="button" class="btn btn-default ">
						<i class="fa fa-file" aria-hidden="true"></i> Thread <span
							class="sr-only">(current)</span>
					</button>
			</a></li>

		</ul>

		<div class="dropdown">
			<button class="dropbtn btn btn-default"  type="button" >

				RESOURCES <i class="fa fa-caret-down"></i>
			</button>
			<div class="dropdown-content">
				<a class="nav-link" href=${contextPath}/character>Character</a> <a
					class="nav-link" href=${contextPath}/episode>Episode</a> <a
					class="nav-link" href=${contextPath}/location>Location</a>
			</div>
		</div>
	</div>
</nav>