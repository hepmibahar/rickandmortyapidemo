<div class="container">
	<hr>
	<div class="row">
		<br />
		<h4>${pageName}</h4>
	</div>
	<div class="row">
		<hr>
		<b>INFO</b>
		<div class="col">
			<i class="fa fa-file" aria-hidden="true"></i><em class="text-muted">
				  Pages: ${pages}   </em> <i class="fa fa-archive" aria-hidden="true"></i><em
				class="text-muted">Count : ${count}</em>
		</div>
	</div>
	<hr>
</div>