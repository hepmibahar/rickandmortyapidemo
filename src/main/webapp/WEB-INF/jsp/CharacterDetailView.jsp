<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="HeaderMenu.jsp" />
<jsp:include page="PageDetailInfo.jsp" />
<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr class="thead-dark">
					<th><button class="btn btn-primay">
							<i class="fa fa-bars"></i>
						</button></th>
					<th>Id</th>
					<th>Name</th>
					<th>Status</th>
					<th>Species</th>
					<th>Gender</th>
					<th>Created</th>				
				</tr>
			</thead>
			<tbody style="height: 100px;">
				<tr>
					<td></td>
					<td>${Character.id}</td>
					<td>${Character.name}</td>
					<td>${Character.status}</td>
					<td>${Character.species}</td>
					<td>${Character.gender}</td>
					<td>${Character.created}</td>
				</tr>
			</tbody>
			<thead class="center">
				<tr class="center">
					<th><button class="btn">
							<i class="fa fa-slideshare" aria-hidden="true"></i>
						</button> Character Image </th>					
				</tr>
			</thead>
			<tbody style="height: 350px;" class="center">
				<tr>
					<td><img class="center" src="${Character.image}" alt="Trulli" width="300" height="300"></td>							
					</tr>
			</tbody>	
			<thead>
				<tr class="thead-primary">
					<th><button class="btn ">
							<i class="fa fa-map-pin" aria-hidden="true"></i>
						</button>Location Name</th>
					<th>Detail</th>
					<th><button class="btn ">
							<i class="fa fa-archive" aria-hidden="true"></i>
						</button> Origin</th>
					<th>Detail</th>


				</tr>
			</thead>
			<tbody style="height: 100px;">
				<tr>
					<td>${Locations.name}</td>
					<td><button class="btn btn-warning">
							<i class="fa fa-folder"></i>
						</button> <br></td>
						<td>${Origins.name}</td>
					<td><button class="btn btn-warning">
							<i class="fa fa-folder"></i>
						</button> <br></td>

				</tr>
			</tbody>
			<thead>
				<tr class="thead-primary">

					<th><button class="btn ">
							<i class="fa fa-video-camera" aria-hidden="true"></i>
						</button> Episode</th>
					<th>Detail</th>

				</tr>
			</thead>
			<tbody style="height: 350px;">
				<c:forEach items="${Episodes}" var="listItem">
					<tr>
						<td><i class="fa fa-caret-right fa-lg" aria-hidden="true"></i>
							${listItem}</td>
						<td><button class="btn btn-warning">
								<i class="fa fa-folder"></i>
							</button> <br></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="container" style="heigh: 100px">
	<h1></h1>
	<br />
</div>

<jsp:include page="Footer.jsp" />
