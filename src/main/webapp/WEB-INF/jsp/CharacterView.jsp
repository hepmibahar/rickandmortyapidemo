<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="HeaderMenu.jsp" />
<jsp:include page="PageInfo.jsp" />

<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr class="thead-dark">
					<th><button class="btn-success">
							<i class="fa fa-bars"></i>
						</button></th>
					<th>Name
						<a href="/characterSorting/"><button class="btn btn-info">
							<i class="fa fa-sort-alpha-asc" aria-hidden="true"></i>
						</button></a>
					</th>
					<th>Status</th>
					<th>Species</th>
					<th>Gender</th>
					<th>Created</th>
					<th>Image</th>
					<th>Detail</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${CharacterList}" var="i">
					<tr>
						<td>${i.id}</td>
						<td>${i.name}</td>
						<td>${i.status}</td>
						<td>${i.species}</td>
						<td>${i.gender}</td>
						<td>${i.created}</td>
						<td>
							<div id="myModal+${i.id}" class="modal fade" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											<img class="center" src="${i.image}">
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>

							<button class="btn btn-danger" data-toggle="modal"
								data-target="#myModal+${i.id}">
								<i class="fa fa-camera-retro" aria-hidden="true"></i>
							</button> <br>
						</td>
						<td><a href="/characters/${i.id}"><button
									class="btn btn-warning">
									<i class="fa fa-folder"></i>
								</button></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

	<div class="container" style="heigh: 100px">
		<div>
			<div style="float: left">
				<a href="/charactersPrev/"><button class="btn btn-warning">
						<i class="fa fa-arrow-circle-left">Prev Page</i>
					</button></a>
			</div>
			<div style="float: right">
				<a href="/charactersNext/"><button class="btn btn-warning">
						<i class="fa fa-arrow-circle-right">Next Page</i>
					</button></a>

			</div>
		</div>
	</div>
</div>

<div class="container" style="heigh: 100px">
	<h1></h1>
	<br />
</div>

<jsp:include page="Footer.jsp" />
