package com.egem.controller;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.egem.model.InfoAndLocation;
import com.egem.restService.LocationRestServise;

/**
 * @author Bahar KALKAN
 *
 */
@Controller
public class LocationController {

	@Autowired
	LocationRestServise rest;

	InfoAndLocation mdl;

	/**InfoAndLocation Model'i ile seçili url verisi elde edilir. Elde edilen
	 *                        seçili url verisine göre jsp verileri aktarılır.
	 * @return InfoAndLocation
	 */
	@RequestMapping("/locations")
	public ModelAndView getViewLocationList(ModelAndView model) {
		ModelAndView mv = new ModelAndView("LocationView"); /** LocationView sayfasına gider. */

		mdl = new InfoAndLocation(rest.getLocationList().info,
				rest.getLocationList().results); /** LocationView modeli doldurulur. */

		mv.addObject("pageName", "LOCATION");
		mv.addObject("count", mdl.info.count);
		mv.addObject("pages", mdl.info.pages);
		mv.addObject("prev", mdl.info.prev);
		mv.addObject("next", mdl.info.next);

		mv.addObject("LocationList", mdl.getResults());

		return mv;
	}

	/**InfoAndLocation Model'i ile next url verisi elde edilir gerekli jsp verileri aktarılır.
	 * @return InfoAndLocation for next page
	 */
	@RequestMapping("/locationsNext")
	public ModelAndView getViewLocationsNext(ModelAndView model) {
		ModelAndView mv = new ModelAndView("LocationView"); /** LocationView sayfasına gider. */

		mv.addObject("pageName", "LOCATION");

		if (mdl.getInfo().next != null) {
			mdl = new InfoAndLocation(rest.getLocationNextOrPrev(mdl.getInfo().next).info,
					rest.getLocationNextOrPrev(mdl.getInfo().next).results);

			mv.addObject("count", mdl.info.count);
			mv.addObject("pages", mdl.info.pages);
			mv.addObject("LocationList", mdl.getResults());

		} else {
			mdl = new InfoAndLocation(rest.getLocationList().info, rest.getLocationList().results);

			mv.addObject("prev", mdl.info.prev);
			mv.addObject("next", mdl.info.next);
			mv.addObject("LocationList", mdl.getResults());

		}

		return mv;
	}

	/** InfoAndLocation Model'i ile prev url verisi elde edilirjsp verileri aktarılır.
	 * @return InfoAndLocation for prev page
	 */
	@RequestMapping("/locationsPrev")
	public ModelAndView getViewLocationPrev(ModelAndView model) {
		ModelAndView mv = new ModelAndView("LocationView"); /** LocationView sayfasına gider. */

		mv.addObject("pageName", "LOCATION");
		if (mdl.getInfo().prev != null) {

			mdl = new InfoAndLocation(rest.getLocationNextOrPrev(mdl.getInfo().prev).info,
					rest.getLocationNextOrPrev(mdl.getInfo().prev).results);

			mv.addObject("count", mdl.info.count);
			mv.addObject("pages", mdl.info.pages);
			mv.addObject("LocationList", mdl.getResults());
		}

		else {

			mdl = new InfoAndLocation(rest.getLocationList().info, rest.getLocationList().results);

			mv.addObject("prev", mdl.info.prev);
			mv.addObject("next", mdl.info.next);
			mv.addObject("LocationList", mdl.getResults());
		}
		return mv;
	}

	/**
	 * @param id LocationModel detay verilerin listelenmesi için gerekli id bilgisi
	 *           alınır.
	 * @return LocationModel verileri view'e aktarılır.
	 */
	@RequestMapping("/locations/{id}")
	public ModelAndView getViewLocation(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("LocationDetailView"); /** LocationDetailView sayfasına gider. */

		mv.addObject("pageName", "LOCATION");
		mv.addObject("Location", rest.getLocation(id));
		mv.addObject("Residents", rest.getLocation(id).getResidents());
		return mv;
	}

	/**InfoAndLocation Model'i ile seçili url verisi elde edilir Location - name
	 *                        nesnesine göre sıralanır sonra view'e aktarılır.
	 * @return InfoAndLocation
	 */
	@RequestMapping("/locationSorting")
	public ModelAndView getViewLocationSorting(ModelAndView model) {
		ModelAndView mv = new ModelAndView("LocationView"); /** LocationView sayfasına gider. */
		
		/**
		 *  Model nesnesi, başlangıç durumu, next url ve prev url'e göre değişilik göstermektedir.
		 *  Seçili modele göre işlem yapmamıza olanak verir. 
		 */
		mdl.getClass();

		Collections.sort(mdl.getResults());

		mv.addObject("pageName", "LOCATION");
		mv.addObject("count", mdl.info.count);
		mv.addObject("pages", mdl.info.pages);
		mv.addObject("prev", mdl.info.prev);
		mv.addObject("next", mdl.info.next);

		mv.addObject("LocationList", mdl.getResults());

		return mv;
	}

}
