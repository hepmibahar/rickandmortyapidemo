package com.egem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Bahar KALKAN
 *
 */
@Controller
public class MainController {

	@RequestMapping("/")
	public String getView() {
		return "Main";
	}

}
