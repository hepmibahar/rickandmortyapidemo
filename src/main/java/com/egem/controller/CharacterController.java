package com.egem.controller;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.egem.model.InfoAndCharacter;
import com.egem.restService.CharacterRestService;

/**
 * @author Bahar KALKAN
 *
 */

@Controller
public class CharacterController {

	@Autowired
	CharacterRestService rest = new CharacterRestService();

	/**
	 * Anlık değişen veriler için model yapısı kullanıldı, değişkenin anlık tek
	 * nesnesi elde edilir.
	 */
	InfoAndCharacter ctrl;

	/**
	 * @return mv ModelAndView CharacterView jsp e gider Modeli ile ilk url
	 *         verisi elde edilir viewe aktarılır
	 * 
	 */
	@RequestMapping("/characters")
	public ModelAndView getViewCharacter() {
		ModelAndView mv = new ModelAndView("CharacterView"); /** CharacterView sayfasına gider. */

		ctrl = new InfoAndCharacter(rest.getCharacterList().info, rest.getCharacterList().results);

		mv.addObject("pageName", "CHARACTER");
		mv.addObject("count", ctrl.info.count);
		mv.addObject("pages", ctrl.info.pages);
		mv.addObject("CharacterList", ctrl.getResults());

		/** Ekranda gösteriilecek veiler ModelAndView ile jsp'ye aktarıldı. */

		return mv;
	}

	/**
	 * @return mv ModelAndView CharacterView jsp e gider Modeli ile url next
	 *         verisi elde edilir viewe aktarılır
	 */
	@RequestMapping("/charactersNext")
	public ModelAndView getViewCharacterNext() {

		ModelAndView mv = new ModelAndView("CharacterView"); /** CharacterView sayfasına gider. */

		mv.addObject("pageName", "CHARACTER");

		if (ctrl.getInfo().next != null) {
			ctrl = new InfoAndCharacter(rest.getCharacterNextOrPrev(ctrl.getInfo().next).info,
					rest.getCharacterNextOrPrev(ctrl.getInfo().next).results);

			mv.addObject("count", ctrl.info.count);
			mv.addObject("pages", ctrl.info.pages);
			mv.addObject("CharacterList", ctrl.getResults());

		} else {
			ctrl = new InfoAndCharacter(rest.getCharacterList().info, rest.getCharacterList().results);
			/**
			 * Eğer son page'den bir sonraki page'e gitmek istenirse listeleme başa döner.
			 */
			mv.addObject("count", ctrl.info.count);
			mv.addObject("pages", ctrl.info.pages);
			mv.addObject("CharacterList", ctrl.getResults());

		}
		return mv;

	}

	/**
	 * @return mv ModelAndView CharacterView jsp e gider Modeli ile url prev
	 *         verisi elde edilir viewe
	 *         aktarılır.
	 * 
	 */
	@RequestMapping("/charactersPrev")
	public ModelAndView getViewCharacterPrev() {
		ModelAndView mv = new ModelAndView("CharacterView");
		/** CharacterView sayfasına gider. */

		mv.addObject("pageName", "CHARACTER");

		if (ctrl.getInfo().prev != null) {

			ctrl = new InfoAndCharacter(rest.getCharacterNextOrPrev(ctrl.getInfo().prev).info,
					rest.getCharacterNextOrPrev(ctrl.getInfo().prev).results);

			mv.addObject("count", ctrl.info.count);
			mv.addObject("pages", ctrl.info.pages);
			mv.addObject("CharacterList", ctrl.getResults());

		} else {
			ctrl = new InfoAndCharacter(rest.getCharacterList().info, rest.getCharacterList().results);

			/**
			 * Eğer son page'den bir sonraki page'e gitmek istenirse listeleme başa döner.
			 */
			mv.addObject("count", ctrl.info.count);
			mv.addObject("pages", ctrl.info.pages);
			mv.addObject("CharacterList", ctrl.getResults());
		}
		return mv;

	}

	/**
	 * @param id CharacaterModel detay verilerin listelenmesi için gerekli id
	 *           bilgisi alınır.
	 * @return CharacaterModel verileri view'e aktarılır.
	 */
	@RequestMapping("/characters/{id}")
	public ModelAndView getViewCharacter(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("CharacterDetailView"); /** CharacterDetailView sayfasına gider. */

		mv.addObject("pageName", "CHARACTER");
		mv.addObject("Character", rest.getCharacter(id));
		mv.addObject("Locations", rest.getCharacter(id).getLocation());
		mv.addObject("Origins", rest.getCharacter(id).getOrigin());

		mv.addObject("Episodes", rest.getCharacter(id).getEpisode());
		return mv;
	}

	/**
	 * @return Modelde bulunan name değişkenine göre sıralama yapılır..
	 */
	@RequestMapping("/characterSorting")
	public ModelAndView getViewCharacterSorting(ModelAndView model) {
		ModelAndView mv = new ModelAndView("CharacterView"); /** CharacterView sayfasına gider. */

		/**
		 * Model nesnesi, başlangıç durumu, next url ve prev url'e göre değişilik
		 * göstermektedir. Seçili modele göre işlem yapmamıza olanak verir.
		 */
		ctrl.getClass();

		Collections.sort(ctrl.getResults());

		mv.addObject("pageName", "CHARACTER");
		mv.addObject("count", ctrl.info.count);
		mv.addObject("pages", ctrl.info.pages);
		mv.addObject("CharacterList", ctrl.getResults());

		/** Ekranda gösteriilecek veiler ModelAndView ile jsp'ye aktarıldı. */

		return mv;
	}

}
