package com.egem.controller;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.egem.model.InfoAndEpisode;
import com.egem.restService.EpisodeRestService;

/**
 * @author Bahar KALKAN
 *
 */
@Controller
public class EpisodeController {

	@Autowired
	EpisodeRestService rest;

	InfoAndEpisode mdl;

	/** InfoAndEpisode Model'i ile seçili url verisi elde edilir view için veriler aktarılır.
	 * @return EpisodeView jsp ekranda gösterilir
	 */
	@RequestMapping("/episodes")
	public ModelAndView getViewEpisodes() {
		ModelAndView mv = new ModelAndView("EpisodeView"); /** EpisodeView sayfasına gider. */

		mdl = new InfoAndEpisode(rest.getEpisodeList().info, rest.getEpisodeList().results);

		mv.addObject("pageName", "EPISODE");
		mv.addObject("count", mdl.info.count);
		mv.addObject("pages", mdl.info.pages);
		mv.addObject("prev", mdl.info.prev);
		mv.addObject("next", mdl.info.next);

		mv.addObject("EpisodeList", mdl.getResults());

		return mv;
	}

	/**InfoAndEpisode Model'i ile next url verisi elde edilir view için veriler aktarılır.
	 * @return EpisodeView jsp ekranda gösterilir
	 */
	@RequestMapping("/episodesNext")
	public ModelAndView getViewEpisodesNext() {
		ModelAndView mv = new ModelAndView("EpisodeView"); /** EpisodeView sayfasına gider. */

		mv.addObject("pageName", "EPISODE");

		if (mdl.getInfo().next != null) {

			mdl = new InfoAndEpisode(rest.getEpisodeNextOrPrev(mdl.getInfo().next).info,
					rest.getEpisodeNextOrPrev(mdl.getInfo().next).results);

			mv.addObject("prev", mdl.info.prev);
			mv.addObject("next", mdl.info.next);

			mv.addObject("count", mdl.info.count);
			mv.addObject("pages", mdl.info.pages);

			mv.addObject("EpisodeList", mdl.getResults());
		} else {

			mdl = new InfoAndEpisode(rest.getEpisodeList().info, rest.getEpisodeList().results);

			mv.addObject("prev", mdl.info.prev);
			mv.addObject("next", mdl.info.next);
			mv.addObject("count", mdl.info.count);
			mv.addObject("pages", mdl.info.pages);
			mv.addObject("EpisodeList", mdl.getResults());
		}

		return mv;
	}

	/**InfoAndEpisode Model'i ile prev url verisi elde edilir view için veriler aktarılır.
	 * @return EpisodeView jsp ekranda gösterilir
	 */
	@RequestMapping("/episodesPrev")
	public ModelAndView getViewEpisodePrev() {
		ModelAndView mv = new ModelAndView("EpisodeView"); /** EpisodeView sayfasına gider. */

		mv.addObject("pageName", "EPISODE");
		if (mdl.getInfo().prev != null) {

			mdl = new InfoAndEpisode(rest.getEpisodeNextOrPrev(mdl.getInfo().prev).info,
					rest.getEpisodeNextOrPrev(mdl.getInfo().prev).results);
			mv.addObject("prev", mdl.info.prev);
			mv.addObject("next", mdl.info.next);
			mv.addObject("count", mdl.info.count);
			mv.addObject("pages", mdl.info.pages);

			mv.addObject("EpisodeList", mdl.getResults());
		}

		else {

			mdl = new InfoAndEpisode(rest.getEpisodeList().info, rest.getEpisodeList().results);

			mv.addObject("prev", mdl.info.prev);
			mv.addObject("next", mdl.info.next);
			mv.addObject("count", mdl.info.count);
			mv.addObject("pages", mdl.info.pages);
			mv.addObject("EpisodeList", mdl.getResults());
		}
		return mv;
	}

	/**
	 * @param id EpisodeModel detay verilerin listelenmesi için gerekli id bilgisi
	 *           alınır.
	 * @return EpisodeModel verileri view'e aktarılır.
	 */
	@RequestMapping("/episodes/{id}")
	public ModelAndView getViewEpisode(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("EpisodeDetailView"); /** EpisodeDetailView sayfasına gider. */

		mv.addObject("pageName", "EPISODE");
		mv.addObject("Episode", rest.getEpisode(id));
		mv.addObject("Characters", rest.getEpisode(id).getCharacters());
		return mv;
	}

	/**InfoAndEpisode Model'i ile seçili url verisi elde edilir Episode - name
	 *                       değişkenine göre sıralanır ve view'e aktarılır.
	 * @return InfoAndEpisode
	 */
	@RequestMapping("/episodeSorting")
	public ModelAndView getViewEpisodesSorting() {
		ModelAndView mv = new ModelAndView("EpisodeView"); /** EpisodeView sayfasına gider. */

		/**
		 *  Model nesnesi, başlangıç durumu, next url ve prev url'e göre değişilik göstermektedir.
		 *  Seçili modele göre işlem yapmamıza olanak verir. 
		 */
		mdl.getClass();

		Collections.sort(mdl.getResults());

		mv.addObject("pageName", "EPISODE");
		mv.addObject("count", mdl.info.count);
		mv.addObject("pages", mdl.info.pages);
		mv.addObject("prev", mdl.info.prev);
		mv.addObject("next", mdl.info.next);

		mv.addObject("EpisodeList", mdl.getResults());

		return mv;
	}
}
