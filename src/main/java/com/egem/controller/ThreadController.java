package com.egem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.egem.model.InfoAndCharacter;
import com.egem.restService.CharacterRestService;
import com.egem.thread.AlphabetFrequency;

/**
 * @author Bahar KALKAN
 *
 */
@Controller
public class ThreadController {

	public String allNameToString = "";

	
	/**
	 * CharacterRestService 
	 * BigO=>O(n) rest nesnesi her çağırıldığında 1,2,3....n
	 */
	@Autowired
	CharacterRestService rest = new CharacterRestService();

	InfoAndCharacter mdl;

	/**
	 * @return ThreadView e aktarılan içerik HashMap<String, int> türündedir.
	 */
	@RequestMapping("/thread-demo")
	public ModelAndView getViewCharacter(ModelAndView model) {
		ModelAndView mv = new ModelAndView("ThreadView"); /** ThreadView sayfasına gider. */

		allNameToString = "";
		AlphabetFrequency alpFreq = new AlphabetFrequency();
		mdl = new InfoAndCharacter(rest.getCharacterList().info, rest.getCharacterList().results);

		/**
		 * Alınan model de gelen verilerden name parametresi bi string değişkene
		 * aktarılır. 
		 * * BigO=>O(n)  n=mdl.results.size()
		 */
		for (int i = 0; i < mdl.results.size(); i++) {
			allNameToString += mdl.results.get(i).getName().trim();
		}

		alpFreq.totalCount = 0;
		alpFreq.allNameString = allNameToString.toUpperCase();
		/**
		 * Alınan tüm name dizesi string değişkene aktarılır
		 * Değişken thread'a gönderilir ve hangi karakterden kaç tane var
		 * hesaplanır.
		 */
		alpFreq.run();

		mv.addObject("AlphCharacterList", alpFreq.finishedValue);
		mv.addObject("totalCount", alpFreq.totalCount);
		return mv;
	}
}
