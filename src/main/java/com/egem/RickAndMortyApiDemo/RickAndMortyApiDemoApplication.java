package com.egem.RickAndMortyApiDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * @author Bahar KALKAN
 * @since 15.05.2020
 * @version 1.0.0.
 */

@ComponentScan("com")
@SpringBootApplication
// com.* dizinindeki tüm classlara erişim izni
public class RickAndMortyApiDemoApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RickAndMortyApiDemoApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(RickAndMortyApiDemoApplication.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
