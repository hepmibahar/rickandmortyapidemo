package com.egem.thread;

import java.util.HashMap;

/**
 * @author Bahar KALKAN
 *
 */
public class AlphabetFrequency extends Thread {

	/**
	 * @param allNameString  Hesaplanacak string değeri bu değişkene aktarılır.
	 * @param finishedString Hesaplama sonucunda hangi karakterden kaç tane var
	 *                       bilgisi değişkene aktarılır.
	 */
	public String allNameString = "";
	public HashMap<Character, Integer> finishedValue;
	public int totalCount;

	/**
	 * run() Thread çalıştığında yapılması istenen işlemler buraya yazılır
	 */
	@Override
	public void run() {

		HashMap<Character, Integer> eachCharCountMap = new HashMap<Character, Integer>();

		char[] charArray = allNameString.toCharArray();
		for (char c : charArray) {
			if (eachCharCountMap.containsKey(c)) {
				/**
				 * Karakterler alfabede bulunan karakter listesi ile karşılaştırılır, kaç tane
				 * olduğu hesaplanır. Dökümanda 25 karakter denildiği için; Harfler teke
				 * düşürüldü toUpperCase() kullanıldı.
				 */
				eachCharCountMap.put(c, eachCharCountMap.get(c) + 1);
			} else {

				eachCharCountMap.put(c, 1);
			}
			totalCount += 1;
		}

		finishedValue = eachCharCountMap;
		System.out.println(eachCharCountMap);
	}

}
