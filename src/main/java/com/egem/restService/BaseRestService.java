package com.egem.restService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Bahar KALKAN
 *
 */
@RestController
public class BaseRestService {

	/**
	 * Temel url değeri sabit birdeğer olduğu için, kod okunabilirliği açısından bir
	 * string'e atandı.
	 * BigO = > O(1)
	 */
	String baseUrl = "https://rickandmortyapi.com/api/";
	
	/**
	 * RestTemplate Restful servislere ulaşıp response almamızı sağlar.
	 * BigO=>O(n) restTemplate nesnesi her çağırıldığında 1,2,3....n
	 */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * HttpHeaders istemci ve sunucunun istek veya yanıtla birlikte ek bilgiler
	 * iletebilmesini sağlar.
	 * BigO=>O(n) headers nesnesi her çağıldığında 1,2,3....n
	 */
	HttpHeaders headers = new HttpHeaders();


}
