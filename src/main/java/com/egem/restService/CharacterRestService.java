package com.egem.restService;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.egem.model.Character;
import com.egem.model.InfoAndCharacter;

@RestController
public class CharacterRestService extends BaseRestService {

	/**
	 * @param chrctrUrl base url, location url ile özelleştirilmiştir. BigO = > O(1)
	 */
	String chrctrUrl = baseUrl + "character/";

	@RequestMapping(value = "/character")
	public InfoAndCharacter getCharacterList() {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		return restTemplate.getForObject(chrctrUrl, InfoAndCharacter.class);
		/**
		 * @return Dönüş veileri tüm karakter bilgilerinin listesidir. 
		 * BigO=>O(n)
		 */

	}

	@RequestMapping(value = "/characterNext")
	public InfoAndCharacter getCharacterNextOrPrev(String resultUrl) {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		return restTemplate.getForObject(resultUrl, InfoAndCharacter.class);
		/**
		 * @return Dönüş veileri next veya prev urlden gelen karakter bilgilerinin listesidir.
		 *BigO=>O(n)
		 */
	}

	@RequestMapping(value = "/character/{id}")
	public Character getCharacter(int id) {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange(chrctrUrl + id, HttpMethod.GET, entity, Character.class).getBody();
		/**
		 * @return Dönüş veileri karakter id'sine göre detaylandırılır. BigO=>O(n)
		 */
	}

}
