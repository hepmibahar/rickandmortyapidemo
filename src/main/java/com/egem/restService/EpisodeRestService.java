package com.egem.restService;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.egem.model.Episode;
import com.egem.model.InfoAndEpisode;

@RestController
public class EpisodeRestService extends BaseRestService {

	/**
	 * @param episodeUrl base url, location url ile özelleştirilmiştir. BigO=>O(1)
	 */
	String episodeUrl = baseUrl + "episode/";

	@RequestMapping(value = "/episode")
	public InfoAndEpisode getEpisodeList() {

		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange(episodeUrl, HttpMethod.GET, entity, InfoAndEpisode.class).getBody();
		/**
		 * @return Dönüş veileri tüm episode bilgilerinin listesidir. 
		 * BigO=>O(n)
		 */
	}

	@RequestMapping(value = "/episodeNextOrPrev")
	public InfoAndEpisode getEpisodeNextOrPrev(String resultUrl) {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		return restTemplate.getForObject(resultUrl, InfoAndEpisode.class);
		/**
		 * @return Dönüş veileri  next veya prev urlden gelen episode bilgilerinin listesidir. *
		 *         BigO=>O(n)
		 */
	}

	@RequestMapping(value = "/episode/{id}")
	public Episode getEpisode(int id) {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange(episodeUrl + id, HttpMethod.GET, entity, Episode.class).getBody();
		/**
		 * @return Dönüş veileri episode bilgilerinin id parametresine göre listesidir.
		 *         BigO=>O(n)
		 */
	}

}
