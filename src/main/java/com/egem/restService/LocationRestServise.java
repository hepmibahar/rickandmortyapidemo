package com.egem.restService;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.egem.model.InfoAndLocation;
import com.egem.model.Location;

/**
 * @author Bahar KALKAN
 *
 */
@RestController
public class LocationRestServise extends BaseRestService {

	/**
	 * @param locationUrl base url, location url ile özelleştirilmiştir. BigO=>O(1)
	 */
	String locationUrl = baseUrl + "location/";

	@RequestMapping(value = "/location")
	public InfoAndLocation getLocationList() {

		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange(locationUrl, HttpMethod.GET, entity, InfoAndLocation.class).getBody();
		/**
		 * @return Dönüş veileri tüm location bilgilerinin listesidir. * BigO=>O(n)
		 */
	}

	@RequestMapping(value = "/locationNextOrPrev")
	public InfoAndLocation getLocationNextOrPrev(String resultUrl) {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		return restTemplate.getForObject(resultUrl, InfoAndLocation.class);
		/**
		 * @return Dönüş veileri  next veya prev urlden gelen location bilgilerinin listesidir. *
		 *         BigO=>O(n)
		 */
	}

	@RequestMapping(value = "/location/{id}")
	public Location getLocation(int id) {
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange(locationUrl + id, HttpMethod.GET, entity, Location.class).getBody();
		/**
		 * @return Dönüş veileri urlden gelen id parametresine göre location
		 *         bilgileridir. * BigO=>O(n)
		 */
	}
}