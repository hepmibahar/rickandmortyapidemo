package com.egem.model;

import java.util.ArrayList;

/**
 * @author Bahar KALKAN
 *
 */
public class InfoAndEpisode {

	public Info info;
	public ArrayList<Episode> results;

	public InfoAndEpisode() {
		super();
	}

	/**
	 * @param info Her sayfalama işleminde info verisi gereklidir, oluşturulan model
	 * @param results yani Episode modeli ile beraber kullanılır.
	 */
	public InfoAndEpisode(Info info, ArrayList<Episode> results) {
		super();
		this.info = info;
		this.results = results;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public ArrayList<Episode> getResults() {
		return results;
	}

	public void setResults(ArrayList<Episode> results) {
		this.results = results;
	}

}
