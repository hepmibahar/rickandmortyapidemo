package com.egem.model;

/**
 * @author Bahar KALKAN
 *
 */
public class Info {
	public int count;
	public int pages;
	public String next;
	public String prev;

	public Info() {
		super();
	}

	/**
	 * @param count Toplam sayfalanacak sayı
	 * @param pages Bulunduğumuz sayfa sayısı
	 * @param next  Sayfalama işlemi için sonraki sayfa bağlantısı
	 * @param prev  Sayfalama işlemi için önceki sayfa bağlantısı
	 */
	public Info(int count, int pages, String next, String prev) {
		super();
		this.count = count;
		this.pages = pages;
		this.next = next;
		this.prev = prev;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public Object getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

}
