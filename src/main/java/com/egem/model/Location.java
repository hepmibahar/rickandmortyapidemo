package com.egem.model;

import java.util.ArrayList;

/**
 * @author Bahar KALKAN
 *
 */
public class Location implements Comparable<Location> {

	private int id;
	private String name;
	private String type;
	private String dimension;
	private ArrayList<String> residents;
	private String url;
	private String created;

	public Location() {
		super();
	}

	/**
	 * @param id		 Benzersiz değişken
	 * @param name       Konumun adı
	 * @param type       Konumun türü
	 * @param dimension  Konumun bulunduğu boyut
	 * @param residents  Konumda en son görülen karakter listesi
	 * @param url        Konumun kendi uç noktasına bağlantı
	 * @param created    Konumun veritabanında oluşturulduğu saat
	 */
	public Location(int id, String name, String type, String dimension, ArrayList<String> residents, String url,
			String created) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.dimension = dimension;
		this.residents = residents;
		this.url = url;
		this.created = created;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public ArrayList<String> getResidents() {
		return residents;
	}

	public void setResidents(ArrayList<String> residents) {
		this.residents = residents;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Object getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 *Sıralama işlemleri için Character modeli kendi içinde karşılaştırılmıştır.
	 */
	@Override
	public int compareTo(Location o) {
		return getName().compareTo(o.getName());
	}
	
}
