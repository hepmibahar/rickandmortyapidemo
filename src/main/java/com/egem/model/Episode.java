package com.egem.model;

import java.util.ArrayList;

/**
 * @author Bahar KALKAN
 *
 */
public class Episode implements Comparable<Episode> {

	private int id;
	private String name;
	private String air_date;
	private String episode;
	private ArrayList<String> characters;
	private String url;
	private Object created;

	public Episode() {
		super();
	}

	/**
	 * @param id 		  Benzersiz değişken
	 * @param name        Bölümün adı
	 * @param air_date    Bölümün yayınlanma tarihi
	 * @param episode     Bölümün kodu
	 * @param characters  Bölümde görülen karakterlerin listesi
	 * @param url         Bölümün kendi bitiş noktasına bağlantısı
	 * @param created     Bölümün veritabanında oluşturulduğu saat
	 */
	public Episode(int id, String name, String air_date, String episode, ArrayList<String> characters, String url,
			Object created) {
		super();
		this.id = id;
		this.name = name;
		this.air_date = air_date;
		this.episode = episode;
		this.characters = characters;
		this.url = url;
		this.created = created;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAir_date() {
		return air_date;
	}

	public void setAir_date(String air_date) {
		this.air_date = air_date;
	}

	public String getEpisode() {
		return episode;
	}

	public void setEpisode(String episode) {
		this.episode = episode;
	}

	public ArrayList<String> getCharacters() {
		return characters;
	}

	public void setCharacters(ArrayList<String> characters) {
		this.characters = characters;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Object getCreated() {
		return created;
	}

	public void setCreated(Object created) {
		this.created = created;
	}

	/**
	 *Sıralama işlemleri için Episode modeli kendi içinde karşılaştırılmıştır.
	 */
	@Override
	public int compareTo(Episode o) {
		return getName().compareTo(o.getName());
	}
	
}
