package com.egem.model;

import java.util.ArrayList;

/**
 * @author Bahar KALKAN
 *
 */
public class InfoAndCharacter {

	public Info info;
	public ArrayList<Character> results;

	public InfoAndCharacter() {
		super();
	}

	/**
	 * @param info Her sayfalama işleminde info verisi gereklidir, oluşturulan model
	 * @param results yani Character modeli ile beraber kullanılır.
	 */
	public InfoAndCharacter(Info info, ArrayList<Character> results) {
		super();
		this.info = info;
		this.results = results;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public ArrayList<Character> getResults() {
		return results;
	}

	public void setResults(ArrayList<Character> results) {
		this.results = results;
	}

}
