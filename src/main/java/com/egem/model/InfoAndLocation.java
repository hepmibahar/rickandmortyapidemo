package com.egem.model;

import java.util.ArrayList;

/**
 * @author Bahar KALKAN
 *
 */
public class InfoAndLocation {
	public Info info;
	public ArrayList<Location> results;

	public InfoAndLocation() {
		super();
	}

	/**
	 * @param info Her sayfalama işleminde info verisi gereklidir, oluşturulan model
	 * @param results yani Location modeli ile beraber kullanılır.
	 */
	public InfoAndLocation(Info info, ArrayList<Location> results) {
		super();
		this.info = info;
		this.results = results;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public ArrayList<Location> getResults() {
		return results;
	}

	public void setResults(ArrayList<Location> results) {
		this.results = results;
	}

	
}
