package com.egem.model;

import java.util.ArrayList;

/**
 * @author Bahar KALKAN
 *
 */
public class Character implements Comparable<Character> {

	private int id;
	private String name;
	private String status;
	private String species;
	private String type;
	private String gender;
	private Object origin;
	private Object location;
	private String image;
	private ArrayList<String> episode;
	private String url;
	private String created;

	public Character() {
		super();
	}

	/**
	 * @param id	Benzersiz değişken
	 * @param name     Karakterin adı
	 * @param status   Karakterin durumu ('Canlı', 'Ölü' veya 'bilinmiyor')
	 * @param species  Karakterin türleri
	 * @param type     Karakterin türü veya alt türü
	 * @param gender   Karakterin cinsiyeti ('Kadın', 'Erkek', 'Cinsiyetsiz' veya 'bilinmiyor')
	 * @param origin   Adın ve karakterin başlangıç ​​konumuna bağlantı
	 * @param location Karakterin bilinen son konum bitiş noktası
	 * @param image    Karakterin görüntüsü ile bağlantısı
	 * @param episode  Karakterin görüntülendiği bölümlerin listesi
	 * @param url      Karakterin kendi URL uç noktasına bağlantı
	 * @param created  Karakterin veritabanında oluşturulduğu saat
	 */
	public Character(int id, String name, String status, String species, String type, String gender, Object origin,
			Object location, String image, ArrayList<String> episode, String url, String created) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.species = species;
		this.type = type;
		this.gender = gender;
		this.origin = origin;
		this.location = location;
		this.image = image;
		this.episode = episode;
		this.url = url;
		this.created = created;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Object getOrigin() {
		return origin;
	}

	public void setOrigin(Object origin) {
		this.origin = origin;
	}

	public Object getLocation() {
		return location;
	}

	public void setLocation(Object location) {
		this.location = location;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ArrayList<String> getEpisode() {
		return episode;
	}

	public void setEpisode(ArrayList<String> episode) {
		this.episode = episode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 *Sıralama işlemleri için Character modeli kendi içinde karşılaştırılmıştır.
	 */
	@Override
	public int compareTo(Character o) {
		return getName().compareTo(o.getName());
	}
	
}
